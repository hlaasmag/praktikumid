package praktikum3;

import lib.TextIO;

public class Tehisintellekt {
	public static void main(String[] args) {
		System.out.println("Sisesta esimene vanus!");
		int vanus1 = TextIO.getlnInt();
		System.out.println("Sisesta teine vanus!");
		int vanus2 = TextIO.getlnInt();
		if (vanus1 - vanus2 > 5 || vanus2 - vanus1 > 5 ) {
			System.out.println("Ages don't match!");
		}
		if (vanus1 - vanus2 < 5 || vanus2 - vanus1 < 5)	{
			System.out.println("Ages match! Success!");
		}
	}
}
