package praktikum2;

import lib.TextIO;

public class ArvJaGrupp {
	public static void main(String[] args) {
		System.out.println("Palun sisesta inimeste arv ja soovitud grupi suurus");
		int inimesteArv = TextIO.getlnInt();
		int grupiSuurus = TextIO.getlnInt();
		
		System.out.println("Saab moodustada " + inimesteArv / grupiSuurus +" gruppi"+", üle jääb "+inimesteArv % grupiSuurus+ " inimene");
	
		//Iluskommentaar
		
	}
}
