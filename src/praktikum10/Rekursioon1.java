package praktikum10;

public class Rekursioon1 {
	public static void main(String[] args) {
		System.out.println(astenda(2, 4));
		for (int i = 1; i < 100; i++) {
		   System.out.println(i + "-" + fibonacci(i));
		}   
	}

	public static int astenda(int arv, int aste) {
		if (aste == 1)
			return arv;

		else

			return arv * astenda(arv, aste - 1);

	}

	public static long fibonacci(int n) {
		if (n == 0)
			return 0;
		if (n == 1)
			return 1;

		return fibonacci(n - 1) + fibonacci(n - 2);
	}
}
