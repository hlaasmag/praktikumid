package praktikum10;

import java.security.Timestamp;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class GraafikaN2ide extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Tundmatu loom");
		Group root = new Group();
		Canvas canvas = new Canvas(800, 800);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void joonista(GraphicsContext gc) {
		gc.setFill(Color.rgb(200, 50, 200));
		gc.setStroke(Color.GREEN);
		gc.setLineWidth(10);
		gc.fillRoundRect(50, 50, 700, 700, 40, 40);

		for (int i = 230; i < 600; i += 30) {
			gc.strokeOval(i, i, 50, 50);
		}
		// Jälg
		gc.setStroke(Color.BROWN);
		gc.strokeOval(100, 450, 150, 250);
		gc.strokeOval(100, 350, 60, 100);
		gc.strokeOval(193, 350, 60, 100);
		// Loom
		gc.setStroke(Color.BLACK);
		gc.strokeOval(490, 240, 150, 200);
		gc.strokeOval(510, 130, 98, 110);
		gc.strokeOval(470, 270, 30, 30);
		gc.strokeOval(630, 270, 30, 30);

		gc.strokeLine(525, 139, 540, 109);
		gc.strokeLine(540, 109, 565, 130);
		gc.strokeLine(560, 132, 585, 105);
		gc.strokeLine(585, 105, 600, 144);

		gc.setStroke(Color.BLUE);
		gc.strokeOval(530, 150, 20, 20);
		gc.strokeOval(567, 150, 20, 20);

		gc.setStroke(Color.rgb(200, 200, 200));
		gc.strokeOval(552, 182, 10, 10);

		gc.setStroke(Color.RED);
		gc.setLineWidth(10);
		gc.strokeLine(537, 215, 577, 215);

		gc.setFill(Color.RED);
		gc.setLineWidth(30);

		Font test = new Font(STYLESHEET_CASPIAN, 15);
		gc.setFont(test);
		gc.fillText("Olen haruldane loom nimega Enn ja siin on minu suurendatud käpajälg!", 220, 780);

		// Päike
		for (int n = 0; n < 34; n++) {
			gc.rotate(11);
			gc.setStroke(Color.YELLOW);
			gc.strokeOval(30, 80, 20, 20);

		}

		for (int n = 0; n < 34; n++) {
			gc.rotate(11);
			gc.setStroke(Color.YELLOW);
			gc.strokeOval(60, 130, 20, 20);

		}

		for (int n = 0; n < 34; n++) {
			gc.rotate(11);
			gc.setStroke(Color.YELLOW);
			gc.strokeOval(89, 180, 20, 20);

		}

	}
}