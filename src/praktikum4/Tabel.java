package praktikum4;

public class Tabel {

	public static void main(String[] args) {
		final int x_size = 7; 
		final int y_size = 7;  
		
	
		for (int x = 0; x <= x_size; x++) {
			for (int y = 0; y <= y_size; y++) {
				if (x == y) {
					System.out.printf("1 ");
				} else {
					System.out.printf("0 ");
				}
				
			}
			
			System.out.printf("%n");
		}
	}
}