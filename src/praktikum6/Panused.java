package praktikum6;

import lib.TextIO;
import praktikum5.Meetodid;
import praktikum5.Vahemik;

public class Panused {

	public static void main(String[] args) {

        int kasutajaRaha = 100;
        System.out.println("Sisesta panus (max 25)");
        int panuseSuurus = Meetodid.kasutajaSisestus(1, 25);
        kasutajaRaha -= panuseSuurus;
        
        int myndiVise = ArvamisM2ng.suvalineArv(0, 1);
        if (myndiVise == 0) {
            System.out.println("Võitsid, saad raha topelt tagasi! Jee!");
            kasutajaRaha += panuseSuurus * 2;
        } else {
            System.out.println("Kaotasid...");
        }
        System.out.println("Sul on " + kasutajaRaha + " raha.");
        
        while (true) {
            //...
        }
	}

	public static int kasutajaSisestus(int min, int max) {
        while (true) {
            System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
            int sisestus = TextIO.getlnInt();
            if (sisestus >= min && sisestus <= max) {
                return sisestus;
            } else {
                System.out.println("Vigane sisestus! Proovi uuesti.");
            }
}

// Kirjutada panustega kulli ja kirja mäng.
// Kasutajale antakse 100 raha.
// Küsitakse panuse suurust, maksimaalne panus on 25 raha.
// Visatakse münt.
// Kui tuli kiri, saab kasutaja panuse topelt tagasi.
// Kui tuli kull, ei saa ta midagi.
// Mäng kestab seni, kuni kasutajal raha otsa saab.
// NB! Mõistlik on kasutajale raha jääki vahel näidata ka.