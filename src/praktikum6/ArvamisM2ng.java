package praktikum6;

import lib.TextIO;

public class ArvamisM2ng {

	public static void main(String[] args) {
		int arvutiArv = suvalineArv(1,100);
		
		
		while (true) {
			System.out.println("Arva ära, mis number on?");
		
			int kasutajaArv = TextIO.getlnInt();
			if (arvutiArv == kasutajaArv) {	
				System.out.println("Arvasid ära! Jess!");
				break;
			} else if (kasutajaArv < arvutiArv) {
				System.out.println("Vale, õige arv on suurem");
		
			} else if (kasutajaArv > arvutiArv) {
				System.out.println("Vale, õige arv on väiksem");
			}
			
		}
		
		
		 
		
			
		

	}

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik) + min;
	}
}

// Arvuti "mõtleb" ühe arvu ühest sajani.
// Küsib kasutajalt, mis number oli.
// Kui kasutaja vastab valesti, siis ütleb kas arvatud number oli suurem või
// väiksem.
// Programm küsib vastust senikaua kuni kasutaja numbri õigesti ära arvab.
// int tagastusV22rtus = suvalineArv(2, 100);
// System.out.println("Meetod tagastas: " + tagastusV22rtus);