package praktikum5;

import lib.TextIO;

public class Kuup{

    public static void main(String[] argumendid) {

        System.out.println("Palun siesta arv");
        int arv = TextIO.getlnInt();
        int arvKuubis = kuupi(arv);
        System.out.println(arvKuubis);
        System.out.println(kuupi(7));
    }

    public static int kuupi(int sisendV22rtus) {
        int tagastusV22rtus = (int) Math.pow(sisendV22rtus, 3);
        return tagastusV22rtus;
    }
    
    
    
    
}

//Kirjutada meetod, mis saab parameetritena kaks arvu (vahemiku) ning tagastab kasutaja sisestatud arvu, juhul kui see on lubatud vahemikus. Senikaua, kui kasutaja sisestab midagi ebasobivat, käseb meetod kasutajal arvu uuesti sisestada.
//Selle meetodi signatuur võib olla näiteks järgmine:

//public static int kasutajaSisestus(int min, int max)