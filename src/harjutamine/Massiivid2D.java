package harjutamine;

public class Massiivid2D {
	
	    public static void main(String[] args){

	        int[][] maatriks = muster(6);

	        //printimine
	        for(int[] row : maatriks){
	            for(int col : row){
	                System.out.printf("%3d ", col);
	            }
	            System.out.println();
	        }

	    }

	    public static int[][] muster (int n){
	        int[][] maatriks = new int[n][n];
	        for(int i = 0; i < n; i++){
	            for(int j = 0; j < n; j++){
	                maatriks[i][j] = (int) Math.pow(i + j, 2);
	            }
	        }

	        return  maatriks;
	    }
	}

